const express = require('express');
const nanoid = require('nanoid');
const Link = require('../models/Link');

const router = express.Router();

const createRouter = () => {
  router.get('/:shortUrl', (req, res) => {
    Link.find({shortUrl: req.params.shortUrl})
      .then(results => res.status(301).redirect(results[0].originalUrl))
      .catch(() => res.sendStatus(500));
  });

  router.post('/links', (req, res) => {
    const saveData = () => {
      const linkData = {
        originalUrl: req.body.url,
        shortUrl: nanoid(7)
      };

      const link = new Link(linkData);

      link.save()
        .then(result => res.send(result))
        .catch(() => saveData());
    };

    saveData();

  });

  return router;
};

module.exports = createRouter;